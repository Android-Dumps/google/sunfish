#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from sunfish device
$(call inherit-product, device/google/sunfish/device.mk)

PRODUCT_DEVICE := sunfish
PRODUCT_NAME := lineage_sunfish
PRODUCT_BRAND := google
PRODUCT_MODEL := Pixel 4a
PRODUCT_MANUFACTURER := google

PRODUCT_GMS_CLIENTID_BASE := android-google

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sunfish-user 12 SP2A.220505.002 8353555 release-keys"

BUILD_FINGERPRINT := google/sunfish/sunfish:12/SP2A.220505.002/8353555:user/release-keys
