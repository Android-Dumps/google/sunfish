#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_sunfish.mk

COMMON_LUNCH_CHOICES := \
    lineage_sunfish-user \
    lineage_sunfish-userdebug \
    lineage_sunfish-eng
